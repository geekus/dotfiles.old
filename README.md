## Prestuff
```
brew install tmux
brew install fish
```

# To apply dotfiles to your user dir

## Warning: This will overwrite any existing files with matching name and should be done on a clean install only! 
```
cd ~
git init
git remote add origin https://geekus@bitbucket.org/geekus/dotfiles.git
git fetch
git checkout osx
git reset --hard HEAD
```


```
cd ~/Library/Application\ Support/Sublime\ Text\ 3
rm -Rf Installed\ Packages/
ln -s ~/.sublime3/Installed\ Packages/ "Installed Packages"
cd Packages/
rm -Rf User
ln -s ~/.sublime3/User User
```

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact