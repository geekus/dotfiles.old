function _prompt_pwd --description 'Print the current working directory, NOT shortened to fit the prompt'
  echo $PWD|sed -e 's|/private||' -e "s|^$HOME|~|"
end

function _is_git
    if [ (command git rev-parse --show-toplevel 2> /dev/null) ]
        set -g is_git "True"
    else
        set -e is_git
    end
end

function _git_dir
    set -l git_dir (command git rev-parse --show-toplevel 2> /dev/null)/(command git rev-parse --git-dir 2> /dev/null)
    echo $git_dir
end

function _git_branch_name
    echo (command git symbolic-ref HEAD ^/dev/null | sed -e 's|^refs/heads/||')
end

function _is_git_stashed
    if test -f (_git_dir)"/refs/stash"
        echo "Stashed"
    end
end

function _git_ahead_count
    set -l branch (_git_branch_name)
    set -l ahead (command git rev-list origin/$branch..HEAD --count 2> /dev/null)
    echo $ahead
end

function _git_behind_count
    set -l branch (_git_branch_name)
    set -l behind (command git rev-list HEAD..origin/$branch --count 2> /dev/null)
    echo $behind
end

# function _git_untracked_count
#   echo (command git status --porcelain | grep  "??" | wc -l | sed "s/ //g")
# end

# function _git_modified_count
#   echo (command git status --porcelain | grep  ".[MT]" | wc -l | sed "s/ //g")
# end

# function _git_added_count
#   echo (command git status --porcelain | grep "^[ACDMT][\ MT]\|[ACMT]D" | wc -l | sed "s/ //g")
# end

function _git_status

    set -g _git_untracked_count 0
    set -g _git_added_count 0
    set -g _git_modified_count 0
    set -g _git_dirty_count 0

    # For a table of scenarii, see http://i.imgur.com/2YLu1.png.

    for line in (command git status --porcelain --ignore-submodules)
        [ (command echo $line | grep "^[ACDMT][\ MT]\|[ACMT]D") ] ; and set _git_added_count (math $_git_added_count+1);
        # [[ "$line" == [\ ACMRT]D\ * ]] && (( deleted++ ))
        [ (command echo $line | grep ".[MT]") ] ; and set _git_modified_count (math $_git_modified_count+1);
        # [[ "$line" == R?\ * ]] && (( renamed++ ))
        # [[ "$line" == (AA|DD|U?|?U)\ * ]] && (( unmerged++ ))
        [ (command echo $line | grep -E "^\?\?") ] ; and set _git_untracked_count (math $_git_untracked_count+1);
        set _git_dirty_count (math $_git_dirty_count+1)
    end # To process all lines as a whole, echo $line foreach and | grep here after end

end

function fish_prompt

    _is_git

    # set -l last_status $status
    set -l cyan (set_color cyan)
    set -l yellow (set_color yellow)
    set -l red (set_color red)
    set -l blue (set_color blue)
    set -l green (set_color green)
    set -l white (set_color white)
    set -l normal (set_color normal)

    # if test $last_status = 0
    #     set arrow "$green➜ "
    # else
    #     set arrow "$red➜ "
    # end

    set -l cwd $cyan(_prompt_pwd)
    set -l git_info ""

    if [ $is_git ]

        if [ (_git_branch_name) ]
            set -l git_branch (_git_branch_name)
            set git_info "$git_info$blue git$normal:$green$git_branch"
        end

    end

    echo -n -s $arrow $cwd $git_info" "$red❯$yellow❯$green❯" "

end

function fish_right_prompt

    if [ $is_git ]

        _git_status

        set -l white (set_color -o white)
        set -l cyan (set_color -o cyan)
        set -l yellow (set_color -o yellow)
        set -l red (set_color -o red)
        set -l blue (set_color -o blue)
        set -l green (set_color -o green)
        set -l normal (set_color normal)

        if [ $_git_added_count -gt 0 ]
            set -l added "$green✚"
            set git_icons "$added"
        end

        if [ $_git_modified_count -gt 0 ]
            set -l modified "$blue✱"
            set git_icons "$git_icons $modified"
        end

        if [ (_is_git_stashed) ]
            set -l stashed "$cyan✭"
            set git_icons "$git_icons $stashed"
        end

        if [ (_git_ahead_count) -gt 0 ]
            set -l ahead "$yellow⬆"
            set git_icons "$git_icons $ahead"
        end

        if [ (_git_behind_count) -gt 0 ]
            set -l behind "$yellow⬇"
            set git_icons "$git_icons $behind"
        end

        if [ $_git_untracked_count -gt 0 ]
            set -l untracked "$white◼"
            set git_icons "$git_icons $untracked"
        end

        echo $git_icons" "

    end

    # Set git-info parameters.
    # zstyle ':prezto:module:git:info' verbose 'yes'
    # zstyle ':prezto:module:git:info:action' format ':%%B%F{yellow}%s%f%%b'
    ### zstyle ':prezto:module:git:info:added' format ' %%B%F{green}✚%f%%b'
    ### zstyle ':prezto:module:git:info:ahead' format ' %%B%F{yellow}⬆%f%%b'
    ### zstyle ':prezto:module:git:info:behind' format ' %%B%F{yellow}⬇%f%%b'
    # zstyle ':prezto:module:git:info:branch' format ':%F{green}%b%f'
    # zstyle ':prezto:module:git:info:commit' format ':%F{green}%.7c%f'
    # zstyle ':prezto:module:git:info:deleted' format ' %%B%F{red}✖%f%%b'
    ### zstyle ':prezto:module:git:info:modified' format ' %%B%F{blue}✱%f%%b'
    # zstyle ':prezto:module:git:info:position' format ':%F{red}%p%f'
    # zstyle ':prezto:module:git:info:renamed' format ' %%B%F{magenta}➜%f%%b'
    ### zstyle ':prezto:module:git:info:stashed' format ' %%B%F{cyan}✭%f%%b'
    # zstyle ':prezto:module:git:info:unmerged' format ' %%B%F{yellow}═%f%%b'
    ### zstyle ':prezto:module:git:info:untracked' format ' %%B%F{white}◼%f%%b'

end
